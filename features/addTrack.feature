# language: ru

Функция: Создание нового трэка
  Находясь на сайте под пользователем role = admin
  Добавляю новый трэк
  Данные должны сохраниться в базе

  Сценарий: Успешное создание нового артиста
    Допустим я нахожусь на странице "/login"
    Если я ввожу "admin" в поле "username"
    И я ввожу "1@345qWert" в поле "password"
    И я нажимаю на кнопку "loginBtn"
    То я нахожусь на странице "/"
    И я нажимаю на ссылку "new_track"
    И я нахожусь на странице "/add/new_track"
    Если я ввожу "testTrack" в поле "title-track"
    И я ввожу "3:30" в поле "duration-tack"
    И я ввожу "2" в поле "trackNum-tack"
    И я выбираю опцию "option-album" "Positions"
    И я нажимаю на кнопку "btn-track"
    То я перехожу на страницу "/"