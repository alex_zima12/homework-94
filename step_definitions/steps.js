const { I } = inject();
// const execSync = require("child_process").execSync;

// Before(() => {
//   execSync("cd ../ && NODE_ENV=test node fixtures.js");
// });

Given('я нахожусь на странице {string}', (page) => {
  I.amOnPage(page);
});

When('я ввожу {string} в поле {string}', (value, fieldName) => {
  I.fillField({id: fieldName}, value);
});

When('я выбираю опцию {string} {string}', (id, value) => {
  I.selectOption(`select[id=${id}]`, value);;
});

When('я выбираю файл {string}', (id) => {
  I.attachFile(`input[id=${id}]`, './images/test.jpg')});

When('я нажимаю на кнопку {string}', (button) => {
  I.click({id: button});
});

When('я нажимаю на ссылку {string}', (link) => {
  I.click({id: link});
});

Then('я перехожу на страницу {string}', (page) => {
  I.amOnPage(page);
});
